package com.uqai.facebook.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "facebook")
public class FacebookPropertiesConfig {

    private String appId;
    private String configId;
    private String appSecret;
    private String authorizeUrl;
    private String url;
    private String redirectUrl;
}