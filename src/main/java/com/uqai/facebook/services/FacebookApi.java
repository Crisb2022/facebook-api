package com.uqai.facebook.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.uqai.facebook.config.FacebookPropertiesConfig;
import com.uqai.facebook.constants.FacebookConstants;
import com.uqai.facebook.dto.FacebookAccessToken;
import com.uqai.facebook.dto.FacebookAccount;
import com.uqai.facebook.dto.FacebookPermissions;
import com.uqai.facebook.utils.HttpUtils;
import jakarta.annotation.PostConstruct;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Slf4j
@Service
@RequiredArgsConstructor
public class FacebookApi {

    public static final String ACCESS_TOKEN = "access_token";
    public static final String STATUS = "status";

    private final RestTemplate restTemplate;
    private final FacebookPropertiesConfig facebookPropertiesConfig;

    private String accessTokenUrl;
    private String meUrl;

    private final FacebookAccessToken facebookAccessToken;

    @PostConstruct
    private void init() {
        accessTokenUrl = facebookPropertiesConfig.getUrl() + "/oauth/access_token";
        meUrl = facebookPropertiesConfig.getUrl() + "/me";
    }

    public String getCallBackUrl(HttpServletRequest request) {

        String callback = HttpUtils.callbackURL(request, FacebookConstants.FACEBOOK_ADMIN_BASE_URL);
        return UriComponentsBuilder.fromHttpUrl(facebookPropertiesConfig.getAuthorizeUrl()).queryParam("client_id", facebookPropertiesConfig.getAppId())
                .queryParam("state", "inti-122341")
                .queryParam("config_id", facebookPropertiesConfig.getConfigId())
                .queryParam("redirect_uri", callback)
                .toUriString();
    }

    public String generateAccessToken(HttpServletRequest request, String code) {

        String callback = HttpUtils.callbackURL(request, FacebookConstants.FACEBOOK_ADMIN_BASE_URL);
        UriComponentsBuilder urlAccessTokenMeta = UriComponentsBuilder.fromHttpUrl(accessTokenUrl)
                .queryParam("client_id", facebookPropertiesConfig.getAppId())
                .queryParam("client_secret", facebookPropertiesConfig.getAppSecret())
                .queryParam("redirect_uri", callback)
                .queryParam("code", code);

        try {
            var result = restTemplate.exchange(urlAccessTokenMeta.toUriString(), HttpMethod.POST, HttpUtils.headers(MediaType.TEXT_HTML), ObjectNode.class);
            var body = result.getBody();
            if (Objects.nonNull(body)) {
                this.facebookAccessToken.setToken(body.path(ACCESS_TOKEN).asText());
                this.facebookAccessToken.setTokenType(body.path("token_type").asText());
                this.facebookAccessToken.setExpiresIn(body.path("expires_in").asLong());
                log.info("Token generated successfully");
            }
            var longDuration = generateAccessTokenLongDuration();
            log.info("Long Token generated successfully {}", longDuration);
            return this.facebookAccessToken.getToken();
        } catch (Exception ex) {
            log.error("Failed to obtain Facebook access token ", ex);
        }
        return null;
    }

    public String generateAccessTokenLongDuration() {
        UriComponentsBuilder urlAccessTokenMeta = UriComponentsBuilder.fromHttpUrl(accessTokenUrl)
                .queryParam("client_id", facebookPropertiesConfig.getAppId())
                .queryParam("client_secret", facebookPropertiesConfig.getAppSecret())
                .queryParam("grant_type", FacebookConstants.SCOPES)
                .queryParam("fb_exchange_token", this.facebookAccessToken.getToken());
        try {
            var result = restTemplate.exchange(
                    urlAccessTokenMeta.toUriString(),
                    HttpMethod.GET,
                    HttpUtils.headers(MediaType.APPLICATION_JSON),
                    ObjectNode.class);
            var body = result.getBody();
            if (Objects.nonNull(body)) {
                this.facebookAccessToken.setToken(body.path(ACCESS_TOKEN).asText());
                this.facebookAccessToken.setTokenType(body.path("token_type").asText());
                this.facebookAccessToken.setExpiresIn(body.path("expires_in").asLong());

            }
            log.info("Long Duration Token generated successfully");
            return this.facebookAccessToken.getToken();
        } catch (Exception ex) {
            log.error("Failed to obtain Facebook access token ", ex);
        }
        return null;
    }

    public Map<String, String> getMe() {
        Map<String, String> meData = new HashMap<>();
        UriComponentsBuilder urlAccessMeInfo = UriComponentsBuilder.fromHttpUrl(meUrl).queryParam(ACCESS_TOKEN, this.facebookAccessToken.getToken());
        try {
            var result = restTemplate.exchange(urlAccessMeInfo.toUriString(), HttpMethod.GET, HttpUtils.headers(MediaType.APPLICATION_JSON), ObjectNode.class);
            var body = result.getBody();
            if (Objects.nonNull(body)) {
                meData.put("id", body.path("id").asText());
                meData.put("name", body.path("name").asText());
            }
            log.info("Info me successfully : {}", body);
            return meData;

        } catch (Exception ex) {
            log.error("Failed to obtain Facebook info data ", ex);
        }
        return meData;
    }

    public List<FacebookPermissions> getPermissions() {
        List<FacebookPermissions> permissions = new ArrayList<>();
        String urlPermissions = meUrl + "/permissions";
        UriComponentsBuilder urlAccessMeInfo = UriComponentsBuilder.fromHttpUrl(urlPermissions)
                .queryParam(ACCESS_TOKEN, this.facebookAccessToken.getToken());
        try {
            var result = restTemplate.exchange(urlAccessMeInfo.toUriString(), HttpMethod.GET, HttpUtils.headers(MediaType.APPLICATION_JSON), ObjectNode.class);
            var body = result.getBody();
            if (ObjectUtils.isEmpty(body)) return permissions;
            var permissionsResponse = body.path("data");
            for (JsonNode permissionItem : permissionsResponse) {
                var permission = permissionItem.path("permission").asText();
                var status = permissionItem.path(STATUS).asText();
                permissions.add(FacebookPermissions.builder().permission(permission).status(status).build());
            }
            return permissions;
        } catch (Exception ex) {
            log.error("Failed to obtain Facebook App permissions ", ex);
        }
        return permissions;
    }


    public Map<String, String> deletePermission(String permissions) {
        Map<String, String> requestStatus = new HashMap<>();
        String urlDeletePermissions = facebookPropertiesConfig.getUrl() + "/me/permissions/" + permissions;
        UriComponentsBuilder urlAccessMeInfo = UriComponentsBuilder.fromHttpUrl(urlDeletePermissions)
                .queryParam(ACCESS_TOKEN, this.facebookAccessToken.getToken());
        try {
            restTemplate.exchange(urlAccessMeInfo.toUriString(), HttpMethod.DELETE, HttpUtils.headers(MediaType.APPLICATION_JSON), ObjectNode.class);
            requestStatus.put(STATUS, "Permission " + permissions + " deleted");
            return requestStatus;
        } catch (Exception ex) {
            log.error("Failed to obtain Facebook App permissions ", ex);
        }
        requestStatus.put(STATUS, "Permission " + permissions + " not deleted");
        return requestStatus;
    }

    public Map<String, String> deleteAllPermissions() {
        Map<String, String> requestStatus = new HashMap<>();
        String urlDeleteAllPermisions = meUrl + "/permissions";
        UriComponentsBuilder urlAccessMeInfo = UriComponentsBuilder.fromHttpUrl(urlDeleteAllPermisions)
                .queryParam(ACCESS_TOKEN, this.facebookAccessToken.getToken());
        try {
            restTemplate.exchange(urlAccessMeInfo.toUriString(), HttpMethod.DELETE, HttpUtils.headers(MediaType.APPLICATION_JSON), ObjectNode.class);
            requestStatus.put(STATUS, "Permissions deleted");
            return requestStatus;
        } catch (Exception ex) {
            log.error("Failed to obtain Facebook App permissions ", ex);
        }
        requestStatus.put(STATUS, "Permission not deleted");
        return requestStatus;
    }

    public List<FacebookAccount> getAccounts() {
        List<FacebookAccount> facebookAccounts = new ArrayList<>();
        String urlUserAccounts = meUrl + "/accounts";
        UriComponentsBuilder urlAccessMeInfo = UriComponentsBuilder.fromHttpUrl(urlUserAccounts)
                .queryParam(ACCESS_TOKEN, this.facebookAccessToken.getToken());
        try {
            var result = restTemplate.exchange(urlAccessMeInfo.toUriString(), HttpMethod.GET, HttpUtils.headers(MediaType.APPLICATION_JSON), ObjectNode.class);
            var body = result.getBody();
            if (ObjectUtils.isEmpty(body)) return facebookAccounts;
            var accountsResponse = body.path("data");

            for (JsonNode account : accountsResponse) {
                var id = account.path("id").asText();
                var name = account.path("name").asText();
                var category = account.path("category").asText();
                var token = account.path(ACCESS_TOKEN).asText();
                facebookAccounts.add(FacebookAccount.builder()
                        .id(id)
                        .name(name)
                        .category(category)
                        .accessToken(token)
                        .categories(getListOfJsonNode(account.path("category_list")))
                        .build());
            }
            return facebookAccounts;
        } catch (Exception ex) {
            log.error("Failed to obtain facebookAccounts of Facebook ", ex);
        }
        return facebookAccounts;
    }

    private List<String> getListOfJsonNode(JsonNode categories) {
        List<String> facebookCategories = new ArrayList<>();
        for (JsonNode category : categories) {
            var name = category.path("name").asText();
            facebookCategories.add(name);
        }
        return facebookCategories;
    }

}
