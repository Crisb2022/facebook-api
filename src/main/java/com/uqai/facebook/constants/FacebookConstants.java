package com.uqai.facebook.constants;

public final class FacebookConstants {

    public static final String ADMIN_BASE_URL = "/api/admin";
    public static final String FACEBOOK_ADMIN_BASE_URL = ADMIN_BASE_URL + "/facebook";
    public static final String FACEBOOK_REQUEST_MATCHERS_ADMIN_URL = ADMIN_BASE_URL + "/**";

    public static final String SCOPES = "fb_exchange_token";


    private FacebookConstants(){}


}
