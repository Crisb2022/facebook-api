package com.uqai.facebook.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Getter
@Builder
@RequiredArgsConstructor
public class FacebookAccount {

    private final String id;
    private final String category;
    private final String name;
    private final List<String> categories;
    private final String accessToken;
}
