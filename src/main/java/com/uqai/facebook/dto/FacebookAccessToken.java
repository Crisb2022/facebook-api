package com.uqai.facebook.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@RequiredArgsConstructor
public class FacebookAccessToken implements Serializable {

    private String token;
    private String tokenType;
    private long expiresIn;
}
