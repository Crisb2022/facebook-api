package com.uqai.facebook.dto;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class FacebookPermissions {
    private final String permission;
    private final String status;
}
