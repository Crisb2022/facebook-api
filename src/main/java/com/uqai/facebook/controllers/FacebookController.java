package com.uqai.facebook.controllers;

import com.uqai.facebook.constants.FacebookConstants;
import com.uqai.facebook.dto.FacebookAccount;
import com.uqai.facebook.services.FacebookApi;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(FacebookConstants.FACEBOOK_ADMIN_BASE_URL)
public class FacebookController {

    private final FacebookApi facebookApi;

    @GetMapping
    public void consumeApi(HttpServletRequest request, HttpServletResponse response) throws IOException {
        var callbackURL = facebookApi.getCallBackUrl(request);
        log.info("Redirect URL: {}", callbackURL);
        response.sendRedirect(callbackURL);
    }

    @GetMapping("/callback-facebook")
    public String generateAccessToken(HttpServletRequest request, @RequestParam(value = "code") String metaCode) {
        return facebookApi.generateAccessToken(request, metaCode);
    }

    @GetMapping("/me")
    public List<FacebookAccount> me() {
        facebookApi.getMe();
        return facebookApi.getAccounts();
    }


}