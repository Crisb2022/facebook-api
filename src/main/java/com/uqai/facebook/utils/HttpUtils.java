package com.uqai.facebook.utils;

import com.fasterxml.jackson.databind.JsonNode;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

public final class HttpUtils {
    private HttpUtils() {
    }

    public static HttpEntity<JsonNode> headers(MediaType mediaType) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(mediaType);
        return new HttpEntity<>(headers);
    }

    public static String getBaseUrlWithProtocol(HttpServletRequest request) {
        String callbackURL = getBaseUrl(request);
        return callbackURL.replace("http:", "https:");
    }

    public static String getBaseUrl(HttpServletRequest request) {
        StringBuilder url = new StringBuilder();
        url.append(request.getRequestURL());
        String uri = request.getRequestURI();
        String contextPath = request.getContextPath();
        return url.substring(0, url.length() - uri.length() + contextPath.length());
    }

    public static String callbackURL(HttpServletRequest request, String facebookPathURL) {
        return HttpUtils.getBaseUrlWithProtocol(request) + facebookPathURL + "/callback-facebook";
    }
}
